class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:get_menu, :get_picture, :show, :edit, :update, :destroy]

  def index
    @restaurants = Restaurant.all.includes(:category)
    @search_form = Restaurant::SearchForm.new
  end

  def search
    @search_form = Restaurant::SearchForm.new(search_form_params)
    @restaurants = @search_form.search
    render 'index'
  end

  def get_search_prefecture_link
    respond_to do |format|
      format.html
      format.js
    end
    render 'restaurants/index/search_prefecture_link'
  end

  def get_search_municipality_link
    prefecture_id = params[:prefecture_id]
    @prefecture = Prefecture.find(prefecture_id)
    @municipalities = @prefecture.municipalities
    respond_to do |format|
      format.html
      format.js
    end
    render 'restaurants/index/search_municipality_link'
  end

  def search_from_municipality
    @restaurants = Restaurant.find_by(municipality_id: params[:municipality_id])
    @search_form = Restaurant::SearchForm.new
    render 'index'
  end

  def search_from_prefecture
    @restaurants = Restaurant.find_by(prefecture_id: params[:prefecture_id])
    @search_form = Restaurant::SearchForm.new
    render 'index'
  end

  def search_from_budget
    @restaurants = Restaurant.where('budget > ?', 1)
    @restaurants.each do |r|
      puts r.budget
    end
    @search_form = Restaurant::SearchForm.new
    render 'index'
  end

  def get_municipality_form
    prefecture_id = params[:prefecture_id]
    prefecture = Prefecture.find(prefecture_id)
    @municipalities = prefecture.municipalities
    respond_to do |format|
      format.html
      format.js
    end
    render 'municipality_form'
  end

  def get_menu
    type_id = params[:type_id]
    @menus = @restaurant.menus.where(type_id: type_id)
    respond_to do |format|
      format.html { redirect_to @restaurant }
      format.js
    end
    render 'menu'
  end

  def get_picture
    type_id = params[:type_id]
    if '0' == type_id
      @pictures = @restaurant.pictures.all
    else
      @pictures = @restaurant.pictures.where(type_id: type_id)
    end
    respond_to do |format|
      format.html { redirect_to @restaurant }
      format.js
    end
    render 'picture'
  end

  def show
    @messages = @restaurant.messages.includes(:restaurant).order(created_at: :desc)
  end

  def new
    @restaurant = Restaurant.new
  end

  def edit
  end

  def create
    @restaurant = Restaurant.new(restaurant_params)
    if @restaurant.save
      @restaurant.send_activation_email
      flash[:success] = "メールを送信しました。アカウントを有効化してください"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if logged_in?
      @current_restaurant.delete
      logout
      flash[:success] = "退会完了しました"
    else
      flash[:warning] = "ログインしていないため退会できません"
    end
    redirect_to root_url
  end

  def activation
    @restaurant = Restaurant.find_by(email: params[:email])
    if @restaurant && !@restaurant.activated? && @restaurant.authenticated?(:activation, params[:id])
      @restaurant.activate
      login @restaurant
      flash[:success] = "アカウントを有効化しました"
      redirect_to root_url
    else
      flash[:danger] = "無効なアクティべーションリンクです"
      redirect_to root_url
    end
  end

  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    def restaurant_params
      params.require(:restaurant).permit(
        :name, :category_id, :image, :email, :profile, :phone_number,
        :prefecture_id, :municipality_id, :address3, :business_hours, :regular_holiday,
        :morning_budget, :noon_budget, :night_budget,
        :home_page, :password, :password_confirmation
      )
    end

    def search_form_params
      params.require(:restaurant_search_form).permit(
        :name, :category_id, :latitude, :longitude, :radius
      )
    end
end
