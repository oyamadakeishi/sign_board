class Restaurant::PicturesController < ApplicationController
  before_action :authenticate, only: [:new, :edit, :update, :create, :destroy]

  def new
    @restaurant_picture = @current_restaurant.pictures.build
  end

  def edit
  end

  def create
    @restaurant_picture = @current_restaurant.pictures.build(restaurant_picture_params)
    if @restaurant_picture.save
      redirect_to root_url
    else
      render 'new'
    end
  end

  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if logged_in?
      @current_restaurant.delete
      logout
      flash[:success] = "退会完了しました"
    else
      flash[:warning] = "ログインしていないため退会できません"
    end
    redirect_to root_url
  end

  private
    def restaurant_picture_params
      params.require(:restaurant_picture).permit(
        :title, :picture, :type_id
      )
    end
end
