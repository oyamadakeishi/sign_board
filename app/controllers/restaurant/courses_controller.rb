class Restaurant::CoursesController < ApplicationController
  before_action :authenticate, only: [:new, :edit, :update, :create, :destroy]

  def new
    @restaurant_course = @current_restaurant.courses.build
  end

  def edit
  end

  def create
    @restaurant_course = @current_restaurant.courses.build(restaurant_course_params)
    if @restaurant_course.save
      redirect_to root_url
    else
      render 'new'
    end
  end

  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if logged_in?
      @current_restaurant.delete
      logout
      flash[:success] = "退会完了しました"
    else
      flash[:warning] = "ログインしていないため退会できません"
    end
    redirect_to root_url
  end

  private
    def restaurant_course_params
      params.require(:restaurant_course).permit(
        :name, :price, :time_limit, :people_num, :bottomless,
        :content, :notes
      )
    end
end
