class Restaurant::MessagesController < ApplicationController
  before_action :set_message, only: [:show, :destroy]
  before_action :authenticate, only: [:new, :create, :destroy]

  def index
    @messages = Message.all
  end

  def show
  end

  def new
    @message = Message.new
  end

  def create
    @message = Message.new(message_params)
    
    if (@message.restaurant.id == @current_restaurant.id) && @message.save
      flash[:success] = "メッセ―ジを送信しました"
      redirect_to root_url
    else
      flash[:warning] = "送信に失敗しました"
      render 'new'
    end
  end

  def destroy
    @message.destroy
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_message
      @message = Message.find(params[:id])
    end

    def message_params
      params.require(:message).permit(
        :restaurant_id, :image, :text
      )
    end
end
