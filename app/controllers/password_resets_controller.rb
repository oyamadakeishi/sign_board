class PasswordResetsController < ApplicationController
  before_action :get_restaurant,                 only:[:create, :edit, :update]
  before_action :valid_restaurant,               only:[:edit, :update]
  before_action :check_expiration,               only:[:edit, :update]
  before_action :check_present_restaurant,       only:[:create]
  before_action :check_activated_restaurant,     only:[:create]
  before_action :check_authenticated_restaurant, only:[:create]

  def new
    @password_forget_form = Restaurant::PasswordForgetForm.new
    render action: 'new'
  end

  def create
    @restaurant.send_password_reset_email
    flash[:success] = "パスワード変更メールを送信しました"
    puts
    puts edit_password_reset_url(@restaurant.password_reset_token, email: @restaurant.email)
    puts
    redirect_to root_url
  end

  def edit
  end

  def update
    if params[:restaurant][:password].empty?
      @restaurant.errors.add(:password, :blank)
      render 'edit'
    elsif @restaurant.update_attributes(restaurant_params)
      login @restaurant
      @restaurant.update_attribute(:password_reset_digest, nil)
      flash.now[:success] = "パスワードを変更しました"
      redirect_to root_url
    else
      render 'edit'
    end
  end


  private
    def restaurant_params
      params.require(:restaurant).permit(:password, :password_confirmation)
    end

    def get_restaurant
      @restaurant = Restaurant.find_by(email: params[:restaurant][:email].downcase)
    end

    def valid_restaurant
      unless (@restaurant && @restaurant.activated? && @restaurant.authenticated?(:password_reset, params[:id]))
        redirect_to root_url
      end
    end

    def check_present_restaurant
      if @restaurant.nil?
        flash[:warning] = "アカウントが存在しません"
        redirect_to action: 'new'
      end
    end

    def check_activated_restaurant
      unless @restaurant.activated?
        @restaurant.send_activation_email
        flash[:warning] = 'アカウントの有効化が行われていません。有効化メールを送信いたしましたので、有効化を行ってください'
        redirect_to root_url
      end
    end

    def check_authenticated_restaurant
      unless @restaurant.authenticated?(:password_reset, params[:id])
        flash[:warning] = 'パスワード変更の権限がありません'
        redirect_to root_url
      end
    end

    def check_expiration
      if @restaurant.password_reset_expired?
        flash[:danger] = "パスワード変更の期限が切れています"
        redirect_to action: 'new'
      end
    end
end
