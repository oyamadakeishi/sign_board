class ApplicationController < ActionController::Base
  include SessionsHelper

  protect_from_forgery with: :exception
  before_action :current_restaurant

  class Forbidden < ActionController::ActionControllerError; end
  class IpAddressRejected < ActionController::ActionControllerError; end

  include ErrorHandlers if Rails.env.production?

  private
    def authenticate
      return if logged_in?
      redirect_to root_path, alert: 'ログインしてください'
    end
end
