class SessionsController < ApplicationController
  before_action :get_restaurant,       only:[:create]
  before_action :present_restaurant,   only:[:create]
  before_action :activated_restaurant, only:[:create]

  def new
    if logged_in?
      redirect_to root_url
    else
      @login_form = Restaurant::LoginForm.new
      render action: 'new'
    end
  end

  def create
    if Restaurant::Authenticator.new(@restaurant).authenticate(params[:restaurant_login_form][:password])
      login @restaurant
      params[:restaurant_login_form][:remember_me] == '1' ? remember(@restaurant) : forget(@restaurant)
      redirect_to root_path
    else
      flash[:warning] = "パスワードが間違っています"
      redirect_to action: 'new'
    end
  end

  def destroy
    logout if logged_in?
    redirect_to root_url
  end

  private
    def get_restaurant
      @restaurant = Restaurant.find_by(email: params[:restaurant_login_form][:email].downcase)
    end

    def present_restaurant
      if @restaurant.nil?
        flash[:warning] = "アカウントが存在しません"
        redirect_to action: 'new'
      end
    end

    def activated_restaurant
      unless @restaurant.activated
        @restaurant.send_activation_email
        #手動テストを開発環境で行うときにメールを送れないためコンソールにパスを出力
        puts activation_url(id: @restaurant.activation_token, email: @restaurant.email)
        flash[:warning] = "アカウントが有効化されていません。有効化メールを送信いたしましたので、有効化を行ってください"
        redirect_to action: 'new'
      end
    end
end
