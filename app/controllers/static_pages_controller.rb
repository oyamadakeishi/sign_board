class StaticPagesController < ApplicationController
  def home
    @search_form = Restaurant::SearchForm.new
    render 'static_pages/home'
  end

  def get_search_prefecture_link
    respond_to do |format|
      format.html
      format.js
    end
    render 'static_pages/home/search_prefecture_link'
  end

  def get_search_municipality_link
    prefecture_id = params[:prefecture_id]
    @prefecture = Prefecture.find(prefecture_id)
    @municipalities = @prefecture.municipalities.page(params[:page])
    respond_to do |format|
      format.html
      format.js
    end
    render 'static_pages/home/search_municipality_link'
  end
end
