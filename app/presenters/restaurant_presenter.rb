class RestaurantPresenter < ModelPresenter
  #delegate :suspended?, :family_name, :given_name, :family_name_kana, :given_name_kana, to: :object

  def text_base_info(name, label_text, options = {})
    markup(:tr, class: name) do |m|
      m << th(label_text)
      m.th(object.name)
    end
  end
end