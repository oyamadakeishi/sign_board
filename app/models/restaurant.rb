class Restaurant < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :password_reset_token, :address3

  belongs_to :category, class_name: "Category", foreign_key: 'category_id'
  belongs_to :prefecture, class_name: "Prefecture", foreign_key: 'prefecture_id'
  belongs_to :municipality, class_name: "Municipality", foreign_key: 'municipality_id'
  has_many :messages, dependent: :destroy
  has_many :pictures, class_name: "RestaurantPicture", dependent: :destroy
  has_many :menus, class_name: "RestaurantMenu", dependent: :destroy
  has_many :courses, class_name: "RestaurantCourse", dependent: :destroy

  geocoded_by      :address
  after_validation :geocode
  reverse_geocoded_by :latitude, :longitude

  before_save :downcase_email
  before_validation :create_address

  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false },
                    allow_nil: true
  validates :password, presence:true, length:{ minimum: 6, maximum: 20 }, allow_nil: true
  validates :profile,         length:{ maximum: 800 }
  validates :phone_number,    length:{ maximum: 100 }
  validates :address,         length:{ maximum: 100 }
  validates :business_hours,  length:{ maximum: 100 }
  validates :regular_holiday, length:{ maximum: 100 }
  validates :home_page,       length:{ maximum: 100 }
  validates :morning_budget, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :noon_budget, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :night_budget, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validate :image_size

  mount_uploader :image, RestaurantImageUploader

  has_secure_password
    class << self
    #渡された文字列のハッシュ値を返す
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end
    #ランダムなトークンを返す
    def new_token
      SecureRandom.urlsafe_base64
    end
  end

  #永続セッションのためにユーザーをデータベースに記憶する
  def remember
    self.remember_token = Restaurant.new_token
    update_attribute(:remember_digest, Restaurant.digest(remember_token))
  end

  #ユーザーのログイン情報を破棄する
  def forget
    update_attribute(:remember_digest, nil)
  end

  #有効化用のメールを送信する
  def send_activation_email
    create_activation_digest
    RestaurantMailer.account_activation(self).deliver_now
  end


  #パスワード再設定のメールを送信する
  def send_password_reset_email
    create_password_reset_digest
    RestaurantMailer.password_reset(self).deliver_now
  end

  #アカウントを有効にする
  def activate
    update_columns(activated: true)
  end

  #パスワード再設定の期限が切れている場合はtrueを返す
  def password_reset_expired?
    self.password_reset_sent_at < 2.hours.ago
  end


  #渡されたトークンがダイジェストと一致したらtrueを返す
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  private
    # アップロードされた画像のサイズをバリデーションする
    def image_size
      if image.size > 3.megabytes
        errors.add(:image, "3MB以上の画像はアップロードできません")
      end
    end

    def downcase_email
      email.downcase!
    end

    def create_address
      #市区町村
      municipalitiy = self.municipality
      prefecture = municipalitiy.prefecture
      self.address = "#{prefecture.name}#{municipalitiy.name}#{self.address3}"
    end

    #パスワード再設定用の属性を設定する
    def create_password_reset_digest
      self.password_reset_token = Restaurant.new_token
      update_columns(password_reset_digest: Restaurant.digest(self.password_reset_token),
                     password_reset_sent_at: Time.zone.now)
    end

    #有効化トークンとダイジェストを作成および代入する
    def create_activation_digest
      self.activation_token = Restaurant.new_token
      update_attribute(:activation_digest, Restaurant.digest(activation_token))
    end
end


