class RestaurantCourse < ApplicationRecord
  belongs_to :restaurant, class_name: "Restaurant", foreign_key: 'restaurant_id'

  validates :name, presence: true, length: { maximum: 50 }
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :time_limit, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :people_num, length: { maximum: 50 }
  validates :bottomless, inclusion: {in: [true, false], allow_blank: true }
  validates :content, length: { maximum: 400 }
  validates :notes, length: { maximum: 400 }
end
