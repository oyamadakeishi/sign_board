class Prefecture < ApplicationRecord
  has_many :restaurants
  has_many :municipalities
  
  validates :name, length:{ maximum: 100 }
end
