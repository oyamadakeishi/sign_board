class RestaurantPicture < ApplicationRecord
  belongs_to :restaurant, class_name: "Restaurant", foreign_key: 'restaurant_id'
  belongs_to :type, class_name: "RestaurantPictureType", foreign_key: 'type_id'

  validates :title, length:{ maximum: 50 }
  validate :picture_size
  
  mount_uploader :picture, RestaurantPictureUploader
  private
    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 3.megabytes
        errors.add(:picture, "3MB以上の画像はアップロードできません")
      end
    end
end
