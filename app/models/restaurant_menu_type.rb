class RestaurantMenuType < ApplicationRecord
  has_many :pictures, class_name: "RestaurantPicture", dependent: :destroy

  validates :name, presence: true, length: { maximum: 50 }
end
