class Category < ApplicationRecord
  has_many :restaurants

  validates :name, length:{ maximum: 100 }
end
