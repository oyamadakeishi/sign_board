class Message < ApplicationRecord
  belongs_to :restaurant, class_name: "Restaurant", foreign_key: 'restaurant_id'

  validates :text, length:{ maximum: 800 }

  mount_uploader :image, MessageImageUploader
end
