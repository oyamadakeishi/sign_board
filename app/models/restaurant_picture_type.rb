class RestaurantPictureType < ApplicationRecord
  has_many :menus, class_name: "RestaurantMenu", dependent: :destroy

  validates :name, presence: true, length: { maximum: 50 }
end
