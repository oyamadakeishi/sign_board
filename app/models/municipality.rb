class Municipality < ApplicationRecord
  has_many :restaurants
  belongs_to :prefecture, class_name: "Prefecture", foreign_key: 'prefecture_id'

  validates :name, length:{ maximum: 100 }
end
