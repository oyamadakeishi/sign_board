class RestaurantMenu < ApplicationRecord
  belongs_to :restaurant, class_name: "Restaurant", foreign_key: 'restaurant_id'
  belongs_to :type, class_name: "RestaurantMenuType", foreign_key: 'type_id'

  validates :name, presence: true, length: { maximum: 50 }
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
