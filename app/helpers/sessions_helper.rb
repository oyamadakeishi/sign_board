module SessionsHelper
  def login(restaurant)
    session[:restaurant_id] = restaurant.id
  end

  def logout
    forget(current_restaurant)
    session.delete(:restaurant_id)
    @current_restaurant = nil
  end

  def remember(restaurant)
    restaurant.remember
    cookies.permanent.signed[:restaurant_id] = restaurant.id
    cookies.permanent[:remember_token] = restaurant.remember_token
  end

  def forget(restaurant)
    restaurant.forget
    cookies.delete(:restaurant_id)
    cookies.delete(:remember_token)
  end

  def current_restaurant
    if (restaurant_id = session[:restaurant_id])
      @current_restaurant ||= Restaurant.find_by(id: restaurant_id)
    elsif (restaurant_id = cookies.signed[:restaurant_id])
      restaurant = Restaurant.find_by(id: restaurant_id)
      if restaurant && restaurant.authenticated?(:password, cookies[:remember_token])
        login restaurant
        @current_restaurant = restaurant
      end
    end
  end

  def logged_in?
    !current_restaurant.nil?
  end
end
