class Restaurant::Authenticator
  def initialize(restaurant)
    @restaurant = restaurant
  end

  def authenticate(token)
    @restaurant.present? &&
      @restaurant.activated &&
      BCrypt::Password.new(@restaurant.password_digest).is_password?(token)
  end
end