class Restaurant::SearchForm
  include ActiveModel::Model
  attr_accessor :name, :category_id, :latitude, :longitude, :radius

  def search
    radius = 2
    restaurants = Restaurant
    if radius.present? && latitude.present? && longitude.present?
      restaurants = restaurants.near([latitude, longitude], radius)
    else
      restaurants = restaurants.all
    end

    if name.present?
      restaurants = restaurants.where("name like ?", "%#{name}%")
    end

    if category_id.present?
      restaurants = restaurants.where(:category_id => category_id)
    end

    return restaurants.includes(:category)
  end
end