# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180628114842) do

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer "restaurant_id"
    t.string "image"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "municipalities", force: :cascade do |t|
    t.string "name", null: false
    t.integer "prefecture_id"
    t.index ["prefecture_id"], name: "index_municipalities_on_prefecture_id"
  end

  create_table "prefectures", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "restaurant_courses", force: :cascade do |t|
    t.integer "restaurant_id"
    t.string "name", null: false
    t.integer "price"
    t.integer "time_limit"
    t.string "people_num"
    t.boolean "bottomless"
    t.string "content"
    t.string "notes"
  end

  create_table "restaurant_menu_types", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "restaurant_menus", force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "type_id"
    t.string "name", null: false
    t.integer "price"
  end

  create_table "restaurant_picture_types", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "restaurant_pictures", force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "type_id"
    t.string "picture", null: false
    t.string "title"
  end

  create_table "restaurants", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "image"
    t.integer "category_id"
    t.string "profile"
    t.string "phone_number"
    t.string "address"
    t.integer "prefecture_id"
    t.integer "municipality_id"
    t.float "latitude"
    t.float "longitude"
    t.string "business_hours"
    t.string "regular_holiday"
    t.integer "morning_budget"
    t.integer "noon_budget"
    t.integer "night_budget"
    t.string "home_page"
    t.string "password_digest"
    t.string "remember_digest"
    t.string "activation_digest"
    t.string "password_reset_digest"
    t.datetime "password_reset_sent_at"
    t.boolean "activated", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["municipality_id"], name: "index_restaurants_on_municipality_id"
    t.index ["prefecture_id"], name: "index_restaurants_on_prefecture_id"
  end

end
