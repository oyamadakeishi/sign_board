class CreateRestaurants < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurants do |t|
      t.string :name, null: false                   #名前
      t.string :email, null: false                  #メールアドレス
      t.string :image                               #画像
      t.integer :category_id                        #カテゴリ
      t.string :profile                             #プロフィール
      t.string :phone_number                        #電話番号
      t.string :address                             #住所
      t.references :prefecture, foreign_key: true   #都道府県
      t.references :municipality, foreign_key: true #市区町村
      t.float  :latitude                            #緯度
      t.float  :longitude                           #経度
      t.string :business_hours                      #営業時間
      t.string :regular_holiday                     #定休日
      t.integer :morning_budget                     #予算(朝)
      t.integer :noon_budget                        #予算(昼)
      t.integer :night_budget                       #予算(夜)
      t.string :home_page                           #ホームページurl
      t.string :password_digest
      t.string :remember_digest
      t.string :activation_digest
      t.string :password_reset_digest
      t.datetime :password_reset_sent_at
      t.boolean :activated, default: false

      t.timestamps
    end
  end
end
