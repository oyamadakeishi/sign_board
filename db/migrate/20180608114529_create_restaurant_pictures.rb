class CreateRestaurantPictures < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurant_pictures do |t|
      t.integer :restaurant_id, foreign_key: true #レストランID
      t.integer :type_id                         #タイプID
      t.string :picture, null: false              #画像
      t.string :title                             #タイトル
    end
  end
end
