class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :restaurant_id #レストラン
      t.string :image          #画像
      t.string :text           #テキスト

      t.timestamps
    end
  end
end
