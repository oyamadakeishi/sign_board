class CreateMunicipalities < ActiveRecord::Migration[5.1]
  def change
    create_table :municipalities do |t|
      t.string :name, null: false                 #名前
      t.references :prefecture, foreign_key: true #都道府県
    end
  end
end
