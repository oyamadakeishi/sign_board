class CreateRestaurantCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurant_courses do |t|
      t.integer :restaurant_id    #レストランID
      t.string :name, null: false #名前
      t.integer :price            #価格
      t.integer :time_limit       #制限時間(単位：分)
      t.string :people_num        #人数
      t.boolean :bottomless       #飲み放題か
      t.string :content           #内容
      t.string :notes             #注意事項
    end
  end
end
