class CreateRestaurantMenus < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurant_menus do |t|
      t.integer :restaurant_id    #レストランID
      t.integer :type_id          #タイプID
      t.string :name, null: false #名前
      t.integer :price            #価格
    end
  end
end
