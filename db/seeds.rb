table_names = %w(category address restaurant restaurant_menu_type restaurant_picture_type restaurant_menu restaurant_picture)
table_names.each do |table_name|
  path = Rails.root.join('db', 'seeds', Rails.env, "#{table_name}.rb")
  if File.exist?(path)
    puts "Creating #{table_name}..."
    require(path)
  end
end