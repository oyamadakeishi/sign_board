types = %w(料理 ランチ ドリンク)

types.each do |t|
  RestaurantMenuType.create!(name: t)
end
