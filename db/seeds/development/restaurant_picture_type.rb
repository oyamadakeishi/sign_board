types = %w(料理 内観 外観 メニュー)

types.each do |t|
  RestaurantPictureType.create!(name: t)
end
