images = %w(basashi bread fish fruits hamburger okonomiyaki pizza spaghetti sumibiyaki yakiniku)

Restaurant.all.each do |r|
  10.times do |i|
    RestaurantPicture.create!(type_id: rand(4)+1,
                              restaurant_id: r.id,
                              picture: File.open("./app/assets/images/#{images[i]}.jpg"),
                              title: images[i])
  end
end