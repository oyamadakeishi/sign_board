categories = %w(日本料理 寿司 魚介海鮮料理 天ぷら 揚げ物 そば うどん ラーメン うなぎ 焼鳥 おでん お好み焼き たこ焼き 郷土料理 
  丼もの ステーキ 鉄板焼き パスタ ピザ ハンバーガー 洋食 フレンチ イタリアン スペイン料理 中華 韓国料理 エスニック カレー
  焼肉 鍋 居酒屋 創作料理 ファミレス カフェ 喫茶店 パン サンドイッチ スイーツ バー 旅館 その他)
names = %w(たろう はなこ じろう よしこ さぶろう)
images = %w(basashi bread fish fruits hamburger okonomiyaki pizza spaghetti sumibiyaki yakiniku)
addresses = ['東京都千代田区一番町６−２２',
             '東京都千代田区一番町８−１１',
             '東京都千代田区一番町１４−１',
             '東京都千代田区三番町 千代田区三番町１２',
             '東京都千代田区一番町２−１',
             '東京都新宿区余丁町１２−３',
             '東京都新宿区四谷１丁目 四谷１丁目１３番',
             '東京都千代田区永田町１丁目１１−２３',
             '東京都港区赤坂３丁目２１−１７',
             '東京都港区元赤坂２丁目１−２']
business_hours = ['昼11:00 ~ 14:00   夜18:00 ~ 22:00',
                  '昼11:30 ~ 14:00   夜17:50 ~ 21:00',
                  '8:00 ~ 20:00',
                  '17:00 ~ 25:00',
                  '18:00 ~ 6:00']
regular_holiday = ['毎週月曜日',
                   '毎月第二火曜  毎週木曜',
                   '無休']
budgets = [500,
           1000,
           2500,
           4000,
           5000,
           7500,
           10000]
profile = "親譲りの無鉄砲で小供の時から損ばかりしている。
小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。
なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。
新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。
弱虫やーい。と囃したからである。
小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。
（青空文庫より）親譲りの無鉄砲で小供の時から損ばかりしている。
小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事"
10.times do |i|
  Restaurant.create!(name: "#{categories[i] + names.sample(1)[0]}",
                     email: "test#{i}@example.com",
                     image: File.open("./app/assets/images/#{images[i]}.jpg"),
                     category_id: i + 1,
                     profile: profile,
                     phone_number: '080-1234-5678',
                     address3: "歌舞伎町",
                     prefecture_id: 13,
                     municipality_id: 658,
                     business_hours: business_hours.sample(1)[0],
                     regular_holiday: regular_holiday.sample(1)[0],
                     morning_budget: budgets.sample(1)[0],
                     noon_budget: budgets.sample(1)[0],
                     night_budget: budgets.sample(1)[0],
                     home_page: 'localhost:3000',
                     password: 'password',
                     password_confirmation: 'password',
                     activated: true,
                     )
  #googlemap apiのリクエスト制限のためディレイ時間をつくる
  sleep(3)
end