#住所データファイル
prefecture_and_municipality_file = File.open("db/seeds/prefecture_and_municipality.csv", "r", :encoding => Encoding::CP932, :invalid => :replace, :undef => :replace)

last_prefecture = ""
last_municipality = ""
prefecture_id = 0

prefecture_and_municipality_file.each do |line|
  record = line.split(",")
  #レコードがなくなったら終了
  if record[0] == ""
    break
  end
  #前レコードと都道府県の重複が無い場合は書き込み処理を行う
  if last_prefecture != record[0]
    prefecture = Prefecture.create!(name: record[0].gsub(/"/, ''))
    prefecture_id = prefecture.id
  end
  #前レコードと市区町村の重複が無い場合は書き込み処理を行う
  if last_municipality != record[1]
    municipality = Municipality.create!(name: record[1].gsub(/"|\n/, ''),
                                        prefecture_id: prefecture_id)
  end
  last_prefecture = record[0]
  last_municipality = record[1]
end



