menus = %w(焼肉 ラーメン チャーハン ナポリタン ハンバーグ ライス ピザ)

Restaurant.all.each do |r|
  7.times do |i|
    RestaurantMenu.create!(type_id: rand(3)+1,
                           restaurant_id: r.id,
                           name: menus[i],
                           price: 100*i)
  end
end