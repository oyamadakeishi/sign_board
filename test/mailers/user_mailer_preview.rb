class RestaurantMailerPreview < ActionMailer::Preview

  def account_activation
    restaurant = Restaurant.first
    Restaurant.activation_token = Restaurant.new_token
    RestaurantMailer.account_activation(restaurant)
  end

  def password_reset
    restaurant = Restaurant.first
    Restaurant.reset_token = Restaurant.new_token
    RestaurantMailer.password_reset(restaurant)
  end
end
