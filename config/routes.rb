Rails.application.routes.draw do
  config = Rails.application.config.sign_board

  root 'static_pages#home'
  resources :static_pages do
    collection do
      get :get_search_prefecture_link
      get :get_search_municipality_link
    end
  end

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/signup', to: 'restaurants#new'
  delete '/signout', to: 'restaurants#destroy'

  get '/restaurants/:id/activation', to: 'restaurants#activation', as: :activation
  
  resources :restaurants do
    collection do
      get :search
      get :search_from_prefecture
      get :search_from_municipality
      get :search_from_budget
      get :get_search_prefecture_link
      get :get_search_municipality_link
      get :get_municipality_form
    end
    member do
      get :get_menu
      get :get_picture
    end
  end
  resources :restaurants,     only: [:new, :show, :index, :edit, :update, :create, :destroy, :activation]
  resources :password_resets, only: [:new, :create, :edit, :update]

  namespace :restaurant, path: config[:restaurant][:path] do
    resources :messages, only: [:new, :show, :index, :create, :destroy], as: :message, controller: :messages
    resources :pictures, only: [:new, :edit, :update, :create, :destroy], as: :picture, controller: :pictures
    resources :menus, only: [:new, :edit, :update, :create, :destroy], as: :menu, controller: :menus
    resources :courses, only: [:new, :edit, :update, :create, :destroy], as: :course, controller: :courses
  end


  get '*anything' => 'errors#routing_error'
end
